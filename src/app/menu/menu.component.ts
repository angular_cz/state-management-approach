import {Component, OnInit} from '@angular/core';
import {Room} from '../model/chat';
import {Store} from '../store/store';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
  rooms: Room[];

  constructor(private store: Store) {

    store.getState()
      .subscribe(state => {
        this.rooms = state.rooms;
      });

  }

  ngOnInit() { }

}
