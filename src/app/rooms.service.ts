import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/toPromise';

import {Room, ChatRoom, Message} from './model/chat';

@Injectable()
export class RoomsService {

  private readonly BASE_URL = 'http://localhost:8080';
  private readonly ROOMS_URL = this.BASE_URL + '/rooms';
  private readonly FAVORITE_ROOMS_URL = this.BASE_URL + '/user/rooms';

  private loadFavorite$ = new Subject();

  public favoriteRoms$ = this.loadFavorite$
    .startWith('init')
    .do(console.log)
    .flatMap(() => this.http.get(this.FAVORITE_ROOMS_URL))
    .map(response => response.json() as Room[])
    .share();

  constructor(private http: Http) { }

  getRooms(): Observable<Room[]> {
    return this.http.get(this.ROOMS_URL)
      .map(response => response.json());
  }

  getRoomById(roomId: number): Observable<ChatRoom> {
    return this.http.get(this.ROOMS_URL + '/' + roomId)
      .map(response => response.json())
  }

  sendMessage(roomId: number, message: Message): Observable<Response> {
    return this.http.post(this.ROOMS_URL + '/' + roomId, message)
  }

  getFavoriteRooms(): Observable<Room[]> {
    return this.favoriteRoms$;
  }

  loadFavoriteRooms() {
    this.loadFavorite$.next('reload');
  }

  addFavorite(roomId: number): Observable<Response> {
    return this.http.post(`${this.FAVORITE_ROOMS_URL}/${roomId}/favorite`, '');
  }

  removeFavorite(roomId: number): Observable<Response> {
    return this.http.delete(`${this.FAVORITE_ROOMS_URL}/${roomId}/favorite`);
  }

}
