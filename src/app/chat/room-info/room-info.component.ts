import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Room} from '../../model/chat';

type Mode = 'edit'
  | 'display';

@Component({
  selector: 'app-room-info',
  templateUrl: './room-info.component.html',
  styleUrls: ['./room-info.component.sass']
})
export class RoomInfoComponent {

  @Input()
  room: Room;

  @Input()
  isFavorite = false;

  @Output()
  addFavorite = new EventEmitter<Room>();

  @Output()
  removeFavorite = new EventEmitter<Room>();

  @Output()
  renameRoom = new EventEmitter<string>();

  private mode: Mode = 'display';

  onFavoriteChange(favoriteState) {
    if (favoriteState) {
      this.addFavorite.emit(this.room);
    } else {
      this.removeFavorite.emit(this.room);
    }
  }

  onNameChange(newName) {
    this.renameRoom.emit(newName);
    this.changeMode();
  }

  changeMode() {
    if (this.mode === 'display') {
      this.mode = 'edit';
    } else {
      this.mode = 'display';
    }
  }

}
