import {Component, OnInit} from '@angular/core';
import {RoomsService} from '../../rooms.service';
import {Room} from '../../model/chat';

import {Store} from '../../store/store'

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.sass']
})
export class RoomsComponent implements OnInit {

  rooms: Room[] = [];
  favoriteRooms: Room[] = [];

  onFavorite: (id: number) => void;
  onUnFavorite: (id: number) => void;

  constructor(private roomService: RoomsService,
              private store: Store) {

    this.onFavorite = store.actionCreator((id) => {
      return {
        type: 'FAVORITE_ADD',
        payload: id
      };
    });

    this.onUnFavorite = store.actionCreator((id) => {
      return {
        type: 'FAVORITE_REMOVE',
        payload: id
      };
    });
  }


  ngOnInit() {
    this.store.getState()
      .subscribe(state => {
        this.rooms = state.rooms;
        this.favoriteRooms = state.favoriteRooms;
      });
  }

  isFavorite(room) {
    return this.favoriteRooms.some(currentRoom => {
      return currentRoom.id === room.id;
    });
  }

  renameRoom(room: Room, newTitle) {

    this.store.dispatch({
      type: 'RENAME_ROOM',
      payload: newTitle,
      roomId: room.id
    });

  }

}
