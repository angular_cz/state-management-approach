import {Component, OnInit} from '@angular/core';
import {RoomsService} from '../../rooms.service';
import {ActivatedRoute} from '@angular/router';
import {toPromise} from 'rxjs/operator/toPromise';
import {Store} from '../../store/store';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html'
})
export class RoomDetailComponent implements OnInit {

  chatRoom: any;

  constructor(private roomService: RoomsService,
              private activatedRoute: ActivatedRoute,
              private store: Store) {

    activatedRoute.params
      .map(params => parseInt(params['id']))
      .subscribe(id => {
        store.dispatch({
          type: 'CHATROOM_LOAD'
        });

        roomService.getRoomById(id)
          .map((chatRoom) => {
              store.dispatch({
                type: 'CHATROOM_LOADED',
                payload: chatRoom
              });
            }
          ).toPromise();
      });

    store.getState()
      .subscribe(state => this.chatRoom = state.chatRoom)


  }

  ngOnInit() { }

}
