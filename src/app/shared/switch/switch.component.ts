import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'ngcz-switch',
  templateUrl: 'switch.component.html',
  styleUrls: ['switch.component.sass']
})
export class SwitchComponent {

  @Input() label = 'switch';
  @Input() state = false;

  @Output() whenSwitch = new EventEmitter<Boolean>(true);

  onChange($event: MouseEvent) {
    $event.preventDefault();

    const checkbox = $event.target as HTMLInputElement;
    this.whenSwitch.emit(checkbox.checked);
  }

}
