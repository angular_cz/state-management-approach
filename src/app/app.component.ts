import {Component} from '@angular/core';
import {RoomsService} from './rooms.service';
import {Store} from './store/store';
import {Effects} from './store/effects';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers: [
    RoomsService,
    Store,
    Effects
  ]
})
export class AppComponent {
  title = 'State management example';
}
