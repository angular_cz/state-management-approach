import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {MenuComponent} from './menu/menu.component';
import {RoomsComponent} from './chat/rooms/rooms.component';
import {RoomDetailComponent} from './chat/room-detail/room-detail.component';
import {RoomSettingsComponent} from './chat/room-settings/room-settings.component';
import {RoomInfoComponent} from './chat/room-info/room-info.component';
import {SwitchComponent} from './shared/switch/switch.component';
import {AppRoutingModule} from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    RoomsComponent,
    RoomDetailComponent,
    RoomSettingsComponent,
    RoomInfoComponent,
    SwitchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
