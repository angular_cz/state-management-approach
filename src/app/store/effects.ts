import {Action} from './app-state';
import {RoomsService} from '../rooms.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class Effects {
  constructor(private roomService: RoomsService) {

  }

  runEffectsFor(action: Action) {
    switch (action.type) {
      case 'INITIALIZATION':
        return this.loadData();

      case 'FAVORITE_ADD':
        return this.addFavorite(action.payload);

      case 'FAVORITE_REMOVE':
        return this.removeFavorite(action.payload);
    }
  }

  private addFavorite(id: number) {
    return this.roomService.addFavorite(id)
      .map(response => ({
        type: 'FAVORITE_CHANGED',
        payload: response.json()
      }));
  }

  private removeFavorite(id: number) {
    return this.roomService.removeFavorite(id)
      .map(response => ({
        type: 'FAVORITE_CHANGED',
        payload: response.json()
      }));
  }

  private loadData() {
    return Observable.combineLatest(
      this.roomService.getRooms(),
      this.roomService.getFavoriteRooms()
    ).map(([rooms, favoriteRooms]) => {

      return {
        type: 'INIT_DATA',
        payload: {
          rooms, favoriteRooms
        }
      };
    });
  }
}
