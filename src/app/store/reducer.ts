import {AppState, Action} from './app-state';
import * as R from 'ramda';

export function reducer(state: AppState, action: Action) {

  switch (action.type) {
    case 'INITIALIZATION':
      return action.payload;

    case 'INIT_DATA':
      return action.payload;

    case 'RENAME_ROOM':
      const index = state.rooms.findIndex(room => room.id === action['roomId']);
      const path = R.lensPath(['rooms', index as any as string, 'title']);

      return R.set(path, action.payload, state);

    case 'CHATROOM_LOAD':
      return Object.assign({}, state,
        {loading: true}
      );

    case 'CHATROOM_LOADED':
      return Object.assign({}, state,
        {
          loading: false,
          chatRoom: action.payload
        }
      );

    case 'FAVORITE_CHANGED':
      return Object.assign({}, state,
        {
          favoriteRooms: action.payload
        }
      );

  }

  return state;
}
