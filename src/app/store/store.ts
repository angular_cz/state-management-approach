import {Observable, Subject, BehaviorSubject, ReplaySubject, AsyncSubject} from 'rxjs';
import {AppState, defaultState, Action} from './app-state';
import {Injectable} from '@angular/core';
import {reducer} from './reducer';
import {Effects} from './effects';


@Injectable()
export class Store {

  private dispatcher$: Subject<Action> = new AsyncSubject();
  private state$: Observable<AppState>;

  constructor(effects: Effects) {

    const initialAction = {
      type: 'INITIALIZATION',
      payload: defaultState
    };

    this.dispatcher$ = new Subject();

    this.state$ = this.dispatcher$
      .startWith(initialAction)
      .flatMap((action) => this.ensureObservable(action))
      .do(() => console.group('Action processing'))
      .scan((oldState: AppState, action: Action) => {

        console.log('old state: ', oldState);
        console.log('action: ', action);

        const newState = reducer(oldState, action);

        setTimeout(() => {
          const nextAction = effects.runEffectsFor(action);
          nextAction && this.dispatch(nextAction);
        }, 1);

        return newState;
      }, null)
      .do((newState) => {
        console.log('new state: ', newState);
        console.groupEnd();
      })
      .publishReplay(1)
      .refCount();
  }

  private ensureObservable(action): Observable<Action> {
    if (action instanceof Observable) {
      return action;
    } else {
      return Observable.of(action);
    }
  }

  getState() {
    return this.state$;
  }

  dispatch(action) {
    this.dispatcher$.next(action);
  }

  actionCreator(func) {
    return (...args) => {
      const action = func(...args);

      this.dispatch(action);

      if (action.payload instanceof Observable) {
        this.dispatch(action.payload);
      }

    };
  }

}
