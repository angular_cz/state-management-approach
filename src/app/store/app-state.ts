import {Room, ChatRoom} from '../model/chat';

export interface AppState {
  rooms: Room[];
  favoriteRooms: Room[];
  chatRoom?: ChatRoom;
}

export interface Action {
  type: string;
  payload?: any;
  [key: string]: any;
}

export let defaultState: AppState = {
  rooms: [
    {
      id: 1,
      title: 'Angular 2 basics from storage',
      description: 'Angular is a framework for building client applications in HTML and either JavaScript or a language like TypeScript that compiles to JavaScript.'
    },
    {
      id: 2,
      title: 'Components',
      description: 'Angular applications are made up of components. \n A component is the combination of an HTML template and a component class that controls a portion of the screen.'
    },
    {
      id: 3,
      title: 'Routing',
      description: 'The Angular Router can interpret a browser URL as an instruction to navigate to a client-generated view.'
    },
    {
      id: 4,
      title: 'DI',
      description: 'Dependency injection is an important application design pattern. Angular has its own dependency injection framework, \n      and we really can\'t build an Angular application without it.'
    },
    {
      id: 5,
      title: 'Observables',
      description: 'How to use observables?'
    },
    {
      id: 6,
      title: 'TypeScript',
      description: 'TypeScript brings you optional static type-checking along with the latest ECMAScript features.'
    },
    {
      id: 7,
      title: 'OffTopic',
      description: 'Everything others ...'
    }
  ],
  favoriteRooms: [
    {
      id: 6,
      title: 'TypeScript',
      description: 'TypeScript brings you optional static type-checking along with the latest ECMAScript features.'
    }
  ]
};
