import {Routes, RouterModule} from '@angular/router';
import {RoomsComponent} from './chat/rooms/rooms.component';
import {RoomDetailComponent} from './chat/room-detail/room-detail.component';
import {RoomSettingsComponent} from './chat/room-settings/room-settings.component';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
  {
    path: 'rooms',
    component: RoomsComponent
  },
  {
    path: 'rooms/:id',
    component: RoomDetailComponent
  },
  {
    path: 'rooms/settings',
    component: RoomSettingsComponent
  },
  {
    path: '',
    redirectTo: '/rooms',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/rooms',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
