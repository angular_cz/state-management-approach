export interface Message {
  user: string;
  text: string;
}

export interface Room {
  id: number;
  title: string;
  description?: string;
}

export interface ChatRoom {
  room: Room,
  messages: Message[]
}
