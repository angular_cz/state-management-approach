import {StateManagementApproachUpdatePage} from './app.po';

describe('state-management-approach-update App', () => {
  let page: StateManagementApproachUpdatePage;

  beforeEach(() => {
    page = new StateManagementApproachUpdatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
