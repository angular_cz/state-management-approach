const restify = require('restify');

const roomsJsonServise = require('./rooms');

const server = restify.createServer();
server.use(restify.CORS());
server.use(restify.bodyParser());

server.get('/', restify.serveStatic({
  directory: __dirname,
  default: 'index.html'
}));

server.get('/rooms', (req, res, next) => {
  res.send(roomsJsonServise.getRoomList());
  next();
});

server.get('/rooms/:id', (req, res, next) => {
  let id = parseInt(req.params.id);

  if (roomsJsonServise.hasRoom(id)) {
    res.send(roomsJsonServise.getRoom(id));
  } else {
    res.send(new restify.ForbiddenError('Room with id ' + id + ' does not exists'));
  }

  next();
});

server.post('/rooms/:id', (req, res, next) => {
  let id = parseInt(req.params.id);

  roomsJsonServise.newMessage(id, req.body);
  console.log('user add new message to#', id, req.body);

  res.send(201);
  next();
});


server.get('/user/rooms', (req, res, next) => {
  res.send(roomsJsonServise.getUserRooms());
  next();
});

server.post('/user/rooms', (req, res, next) => {
  roomsJsonServise.setUserRooms(myRooms);

  res.send(roomsJsonServise.getUserRooms());
  next();
});

server.post('/user/rooms/:id/favorite', (req, res, next) => {
  let id = parseInt(req.params.id);

  if (roomsJsonServise.hasRoom(id)) {
    roomsJsonServise.addUserFavorite(id);
    res.send(roomsJsonServise.getUserRooms());
  } else {
    res.send(new restify.ForbiddenError('Room with id ' + id + ' does not exists'));
  }

  next();
});

server.del('/user/rooms/:id/favorite', (req, res, next) => {
  let id = parseInt(req.params.id);

  if (roomsJsonServise.hasRoom(id)) {
    roomsJsonServise.removeUserFavorite(id);
    res.send(roomsJsonServise.getUserRooms());
  } else {
    res.send(new restify.ForbiddenError('Room with id ' + id + ' does not exists'));
  }

  next();
});

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});

roomsJsonServise.startMessagingRobot();
