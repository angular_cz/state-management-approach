const casual = require('casual');

const rooms = [
  {
    "id": 1,
    "title": "Angular 2 basics",
    description: `Angular is a framework for building client applications in HTML and either JavaScript or a language like TypeScript that compiles to JavaScript.`
  },
  {
    "id": 2,
    "title": "Components",
    description: `Angular applications are made up of components. 
      A component is the combination of an HTML template and a component class that controls a portion of the screen.`
  },
  {
    "id": 3,
    "title": "Routing",
    description: `The Angular Router can interpret a browser URL as an instruction to navigate to a client-generated view.`
  },
  {
    "id": 4,
    "title": "DI",
    description: `Dependency injection is an important application design pattern. Angular has its own dependency injection framework, 
      and we really can't build an Angular application without it.`
  },
  {
    "id": 5,
    "title": "Observables",
    description: `How to use observables?`
  },
  {
    "id": 6,
    "title": "TypeScript",
    description: `TypeScript brings you optional static type-checking along with the latest ECMAScript features.`
  },
  {
    "id": 7,
    "title": "OffTopic",
    description: `Everything others ...`
  }
];

let myRoomsIds = new Set();
myRoomsIds.add(1);

const chats = rooms
  .map(room => {
    return {
      room: room,
      messages: new Array(casual.integer(5, 15))
        .fill(1)
        .map(v => ({
            user: casual.full_name,
            text: casual.sentences(casual.integer(1, 10))
          })
        )
    }
  })
  .reduce((rooms, chatRoom) => {
    rooms[chatRoom.room.id] = chatRoom;

    return rooms;
  }, {});

function newMessage(roomId, message) {
  message = message || casual.sentences(casual.integer(2, 12));

  newMessageObj(roomId, {
    user: casual.full_name,
    text: message
  });
}

function newMessageObj(roomId, messageObj) {

  chats[roomId].messages.pop();
  chats[roomId].messages.unshift(messageObj);

  console.log('add message to #' + roomId);
}

function getRoomById(id) {
  const filterById = (room) => id === room.id;

  return rooms.filter(filterById)[0];
}

exports.newMessageText = newMessage;
exports.newMessage = newMessageObj;

exports.getRoomList = function () {
  return rooms;
};

exports.hasRoom = function (id) {
  return Boolean(chats[id]);
};

exports.getRoom = function (id) {
  if (!chats[id]) {
    throw new Error('Bad parametr ID');
  }

  return chats[id];
};

exports.getUserRooms = function () {
  return [...myRoomsIds].map(getRoomById);
};

exports.setUserRooms = function (myRoomsUpdated) {
  myRoomsUpdated = myRoomsUpdated || [
      rooms[0]
    ];

  const ids = myRoomsUpdated.map(item => item.id);

  myRoomsIds = new Set(ids);
};

exports.addUserFavorite = function (id) {
  myRoomsIds.add(id)
};

exports.removeUserFavorite = function (id) {
  myRoomsIds.delete(id)
};

exports.startMessagingRobot = function () {
  setInterval(() => {
    const rand = casual.integer(1, 10);

    if (rand < 3) {
      newMessage(casual.integer(1, 7));
    }

    if (rand < 6) {
      newMessage(casual.integer(1, 7));
    }

    if (rand < 9) {
      newMessage(casual.integer(1, 7));
    }

  }, 3000);
};



